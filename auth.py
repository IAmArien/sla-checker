import sys as system
from datetime import datetime

class Auth(object):
    def __init__(self, class_pst, class_email_conf):
        super().__init__()
        self.class_pst = class_pst
        self.class_email_conf = class_email_conf
        
    def configure(self, config_file_dict: dict) -> dict:               
        config_file = self.class_email_conf.get_email_config_file(config_file_dict["Email-Monitoring"]["Config-Directory-Name"], config_file_dict["Email-Monitoring"]["Config-File-Extension"])
        if not config_file == None:
            valid_emails = self.class_email_conf.acquire_valid_emails(config_file)
            current_time = self.class_pst.get_current_time()
            if not current_time == None:
                standard_pt = self.class_pst.time_conversion(current_time, config_file_dict["Email-Monitoring"]["Datetime-Format"])                                
                if standard_pt == None:                    
                    system.exit(0)
            else:                
                system.exit(0)
        else:            
            system.exit(0)
        return dict(emails=valid_emails, standard_pt=standard_pt)

    @staticmethod
    def confirm_sla(current_datetime: datetime, email_contents: dict, valid_emails_list: list, configuration_file: dict) -> tuple:        
        if email_contents["load_end_time"] == None:
            return (None)
        if email_contents["load_end_time"] <= current_datetime:
            is_in_valid_emails = False            
            for each_valid_emails in valid_emails_list:                                              
                if email_contents["email"] in each_valid_emails:                   
                    is_in_valid_emails = True                                        
                else:
                    is_in_valid_emails = False                
            if not is_in_valid_emails:
                return ('Invalid Email')
            # MATCH DT THROWN WITH VALID EMAILS LIST SCHEDULE
            # UPDATE ROW FLAG
            # REPORT STATUS VIA NOTEPAD            
        else:
            return ('Load End Time is greater than Current Date Time')  
